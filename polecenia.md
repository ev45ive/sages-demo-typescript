create-react-app . --template=typescript

mkdir -p src/playlists/containers/
mkdir -p src/playlists/components/

touch src/playlists/containers/PlaylistsView.tsx
touch src/playlists/components/PlaylistDetails.tsx
touch src/playlists/components/PlaylistForm.tsx
touch src/playlists/components/PlaylistList.tsx

mkdir -p src/search/containers/
mkdir -p src/search/components/

touch src/search/containers/SearchView.tsx
touch src/search/components/AlbumCard.tsx
touch src/search/components/ArtistCard.tsx
touch src/search/components/SearchForm.tsx


## Auth

```ts
// https://developer.spotify.com/dashboard
token = btoa('<client-id>:<clien-secret>')

fetch('https://accounts.spotify.com/api/token',{
    method:'POST',
    headers:{
        'Content-Type':'application/x-www-form-urlencoded',
        Authorization: "Basic " +token
    },
    body: 'grant_type=client_credentials'
}).then(res => res.json()).then(console.log)
// access_token: "BQBpXe09fO6h2_nAAuWDVC86mv_OqjgupfTqJsbNWXmKjAOTKxBQiC5TL0jcWEmlYNQBCEZJArcMhWS3Qbk"
// expires_in: 3600
// token_type: "Bearer"
```