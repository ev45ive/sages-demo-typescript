/// Klasa ??

// export class Playlist {
// //   private _id: string;

//   public get id(): string {
//     return this._id;
//   }
//   public set id(value: string) {
//     this._id = value;
//   }

//   constructor(private _id: string) {
//     // this.id = id;
//   }
// }

export interface Artist {
  type: "artist";
  id: string;
  name: string;
}
export interface Track {
  type: "track";
  id: string;
  name: string;
  duration: number;
}
export interface Playlist {
  type: "playlist";
  id: string;
  name: string;
  /**
   * Is playlist visible to others
   * https://developer.spotify.com/documentation/general/guides/working-with-playlists/
   */
  public: boolean;
  description: string;
  tracks?: Track[];
}

export const p: Playlist = {
  id: "123",
  type: "playlist",
  name: "playlist",
  description: "",
  public: true,
  //   tracks:[{id:'123'}]
};

// p.tracks.length // Object is possibly 'undefined'.
// if (p.tracks !== undefined) {
if (p.tracks) {
  p.tracks.length; // Object is certainly NOT 'undefined'.
} else {
  p.tracks; // Object is certainly 'undefined'.
}

const len1 = p.tracks ? p.tracks.length : 0; // number
const len2 = p.tracks && p.tracks.length; // number | undefined
const len3 = p.tracks?.length; // number | undefined
const len4 = p.tracks?.length ?? 0; // number

/// null ? undefined??
let maybeString: string | number = "123" as string | number;

if (typeof maybeString === "string") {
  maybeString.toUpperCase();
} else {
  maybeString.toExponential();
}

// let result: Playlist | Track = {} as any
let costam: Date | Array<any> = {} as any;

if (costam instanceof Date) {
  costam.getMilliseconds();
}

type SearchResultType = Playlist | Track /* | Artist */; // Check function completness

function renderResult(result: SearchResultType) {
  if (result.type === "playlist") {
    return `${result.name} tracks ${result.tracks?.length || 0}`;
  }

  if (result.type === "track") {
    return `${result.name} duration ${result.duration}`;
  }

  checkExhaustiveness(result); // Exhaustiveness

  //   const _neverHappens: never = result;
  //   console.log("potem"); // Unreachable code detected.ts(7027)
}

function checkExhaustiveness(result: never): never {
  throw new Error(`Unexpected result ${result}`);
}

// while(true){
//     if(1>2){ break; }
// }
// console.log('potem');

// function renderResult(result: Playlist | Track) {
//   if ("description" in result) {
//     result.tracks;
//   }
//   result.type // "playlist" | "track"

//   switch (result.type) {
//     case "playlist":
//         return `${result.name} tracks ${result.tracks?.length || 0}`;

//     case "track":
//       return `${result.name} duration ${result.duration}`;
//     default:
//       result;
//   }
// }
