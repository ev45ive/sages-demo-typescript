
export const cls = (...classes: (string | boolean)[]): string => {
    return classes.filter(Boolean).join(" ");
};
