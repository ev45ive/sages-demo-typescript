import React from "react";
import { Album } from "../../core/model/Search";

interface Props {
  album: Album;
}

export const AlbumCard = ({ album }: Props) => {
  return (
    <div className="card text-left">
      <img className="card-img-top" src={album.images[0]?.url} />
      <div className="card-body">
        <h4 className="card-title">{album.name}</h4>
        {/* <p className="card-text">Body</p> */}
      </div>
    </div>
  );
};
