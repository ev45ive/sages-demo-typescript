import React, { useState } from "react";

interface Props {
  onSearch(query: string): void;
}

export const SearchForm = ({ onSearch }: Props) => {
  const [query, setQuery] = useState("");

  return (
    <div>
      <div className="input-group">

        <input
          type="text"
          className="form-control"
          name="name"
          id="name"
          value={query}
          onChange={(event) => setQuery(event.currentTarget.value)}
          placeholder="Search"
        />

        <span className="input-group-btn">
          <button
            className="btn btn-secondary"
            type="button"
            aria-label=""
            onClick={() => onSearch(query)}
          >
            Search
          </button>
        </span>
        
      </div>
    </div>
  );
};
