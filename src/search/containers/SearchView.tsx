import axios, { AxiosError } from "axios";
import React, { useState } from "react";
import { p } from "../../core/model/Playlist";
import {
  Album,
  isAlbumSearchResponse,
  SimpleAlbum,
} from "../../core/model/Search";
import { AlbumCard } from "../components/AlbumCard";
import { SearchForm } from "../components/SearchForm";

interface Props {}

const albumsMock: SimpleAlbum[] = [
  {
    id: "123",
    name: "Album 123",
    type: "album",
    images: [{ url: "https://www.placecage.com/c/300/300" }],
  },
  {
    id: "234",
    name: "Album 234",
    type: "album",
    images: [{ url: "https://www.placecage.com/c/400/400" }],
  },
  {
    id: "345",
    name: "Album 345",
    type: "album",
    images: [{ url: "https://www.placecage.com/c/500/500" }],
  },
  {
    id: "456",
    name: "Album 456",
    type: "album",
    images: [{ url: "https://www.placecage.com/600/600" }],
  },
];

interface SpotifyError {
  error: { message: string };
}

function isAxiosError<T>(error: any): error is AxiosError<T> {
  return "isAxiosError" in error;
}

function isSpotifyError(
  error: AxiosError<any>
): error is AxiosError<SpotifyError> {
  return error.response?.data?.error?.message;
}

export const SearchView = (props: Props) => {
  const [results, setResults] = useState<Album[]>([] as Album[]);
  const [message, setMessage] = useState("");

  const searchAlbums = async (query: string) => {
    try {
      const { data } = await axios.get("https://api.spotify.com/v1/search", {
        headers: {
          Authorization:
            "Bearer BQBpXe09fO6h2_nAAuWDVC86mv_OqjgupfTqJsbNWXmKjAOTKxBQiC5TL0jcWEmlYNQBCEZJArcMhWS3Qbk",
        },
        params: {
          type: "album",
          q: query,
        },
      });
      if (isAlbumSearchResponse(data)) {
        setResults(data.albums.items);
      } else {
        throw new Error("Bad response");
      }
    } catch (error: unknown) {
      if (!isAxiosError(error)) {
        return setMessage("Unexpected Error");
      }

      if (error.response && isSpotifyError(error)) {
        setMessage(error.response.data.error.message);
      }
    }
  };

  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm onSearch={searchAlbums} />
        </div>
      </div>
      <div className="row mt-3">
        <div className="col">
          {message && <p className="alert alert-danger">{message}</p>}
          <div className="row row-cols-1 row-cols-sm-4 g-0">
            {results.map((result) => (
              <div className="col">
                <AlbumCard album={result} key={result.id} />
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

// interface ITest extends Object {
//     test: string;
// }

// type objecykeys = keyof Object
// type onlytestKeys = Exclude<keyof ITest,keyof Object>
// const c:onlytestKeys = 'test'

// type TTest2 = {
//     [key in  Exclude<keyof ITest,keyof Object>]: ITest[key];
// };
