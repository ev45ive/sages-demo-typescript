import React from "react";
// import logo from './logo.svg';
// import './App.css';
import "bootstrap/dist/css/bootstrap.css";
import PlaylistsView from "./playlists/containers/PlaylistsView";
import { Redirect, Route, Switch } from "react-router-dom";
import { SearchView } from "./search/containers/SearchView";

function App() {
  return (
    <div className="App">
      <div className="container">
        <div className="row">
          <div className="col">
            <h1>Hello React TypeScript</h1>

            <Switch>
              <Redirect path="/" exact={true} to="/search" />
              <Route path="/playlists" component={PlaylistsView} />
              <Route path="/search" component={SearchView} />
            </Switch>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
