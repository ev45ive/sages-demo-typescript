import React, { useEffect, useState } from "react";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistForm } from "../components/PlaylistForm";
import { PlaylistList } from "../components/PlaylistList";
import { Playlist } from "../../core/model/Playlist";
import { Route, RouteComponentProps } from "react-router-dom";

interface Props extends RouteComponentProps {}

const playlistsData: Playlist[] = [
  {
    id: "123",
    type: "playlist",
    name: "My playlist 123",
    public: true,
    description: "My <3 PLaylist 123",
  },
  {
    id: "234",
    type: "playlist",
    name: "My playlist 234",
    public: false,
    description: "My <3 PLaylist 234",
  },
  {
    id: "345",
    type: "playlist",
    name: "My playlist 345",
    public: true,
    description: "My <3 PLaylist 345",
  },
];

type modes = "edit" | "details";
type PlaylistDetailsUrl = `/playlists/${string}/${modes}/`;
const url: PlaylistDetailsUrl = "/playlists/123/details/";

const PlaylistsView = ({ history: { push } }: Props) => {
  //   const [playlists, setPlaylists] = useState<never[]>([]);
  const [playlists, setPlaylists] = useState(playlistsData);
  const [selectedId, setSelectedId] = useState<string | undefined>();
  const [selectedPlaylist, setSelectedPlaylist] = useState<
    Playlist | undefined
  >();
  const [mode, setMode] = useState<"edit" | "details">("details");

  const selectPlaylist = (id: Playlist["id"]) => {
    const playlist_id = id === selectedPlaylist?.id ? undefined : id;
    push(`/playlists/${playlist_id}/details/`);
  };

  useEffect(() => {
    setSelectedPlaylist(playlists.find((p) => p.id === selectedId));
  }, [selectedId]);

  return (
    <div>
      <div className="row">
        <div className="col">
          <PlaylistList
            playlists={playlists}
            selectedId={selectedId}
            onSelect={selectPlaylist}
          />
        </div>

        <div className="col">
          {/* {selectedPlaylist && <PlaylistDetails playlist={selectedPlaylist} />} */}
          {/* {mode === "details" && <PlaylistDetails playlist={selectedPlaylist} />} */}

          <Route
            path="/playlists/:playlist_id/details/"
            render={({ match }) => {
              setSelectedId(match.params.playlist_id);
              return <PlaylistDetails playlist={selectedPlaylist} />;
            }}
          />

          {/* {mode === "edit" && selectedPlaylist && (
            <PlaylistForm playlist={selectedPlaylist} />
          )} */}
        </div>
      </div>
    </div>
  );
};

export default PlaylistsView;
