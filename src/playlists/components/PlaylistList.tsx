import React from "react";
import { Playlist } from "../../core/model/Playlist";
import { cls } from "../../core/utils/cls";

interface Props {
  playlists: Playlist[];
  selectedId?: Playlist["id"];
  onSelect: (id: Playlist["id"]) => void;
}

export const PlaylistList = ({ playlists, selectedId, onSelect }: Props) => {
    
  // const select = (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
  //     event.currentTarget.href
  // }

  return (
    <div>
      <div className="list-group">
        {playlists.map((p, index) => (
          <a
            href="#"
            onClick={() => onSelect(p.id)}
            className={cls(
              "list-group-item list-group-item-action",
              selectedId === p.id && "active"
            )}
            key={p.id}
          >
            {index + 1}. {p.name}
          </a>
        ))}
      </div>
    </div>
  );
};
