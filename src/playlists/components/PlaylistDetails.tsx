import React from "react";
import { Playlist } from "../../core/model/Playlist";

interface Props {
  playlist?: Playlist;
}

export const PlaylistDetails = ({ playlist }: Props) => {
  if (!playlist) {
    return <p className="alert alert-info">Please select playlist</p>;
  }

  return (
    <div>
      <dl>
        <dt>Name:</dt>
        {/* <dd>{playlist?.name}</dd> */}
        <dd>{playlist.name}</dd>

        <dt>Public:</dt>
        <dd>{playlist.public ? "Yes" : "No"}</dd>

        <dt>Description:</dt>
        <dd>{playlist.description}</dd>
      </dl>
    </div>
  );
};
